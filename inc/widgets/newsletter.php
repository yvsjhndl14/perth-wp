<?php 
/**
	@package perth-project-theme
	widgets/newsletter.php
	===================================
	NEWSLETTER WIDGET
	===================================
*/
namespace Perth_theme\Widget;

/**
 * 
 */
class Newsletter extends \WP_Widget {
	
	public function __construct() {
		$widget_ops = array(
			'classname'	=> '',
			'description' => 'Embed a Newsletter Subscription Form'
		);

		parent::__construct('perth_newsletter', 'Newsletter', $widget_ops);
	}

	public function widget($args, $instance) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$label = isset($instance['label']) ? esc_attr($instance['label']) : 'Newsletter Subscription';

		echo $args['before_widget'];
		?>

		<form action="#">
			<label><?php echo $label; ?></label>
			<input type="email" placeholder="Enter Email"><i class="fas fa-envelope"></i>
		</form>

		<?php

		echo $args['after_widget'];
	}

	public function form($instance) {
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$label = isset($instance['label']) ? esc_attr($instance['label']) : 'Newsletter Subscription';
		?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('label'); ?>"><?php _e('Input Label:'); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('label'); ?>" name="<?php echo $this->get_field_name('label'); ?>" type="text" value="<?php echo $label; ?>">
		</p>

		<?php
	}

	public function update($new_instance, $old_instance) {
		$instance = array();

		$instance['title'] =  (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		$instance['label'] =  (!empty($new_instance['label'])) ? strip_tags($new_instance['label']) : 'Newsletter Subscription';

		return $instance;
	}
}