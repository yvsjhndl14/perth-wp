<?php 
/**
	@package perth-project-theme
	class.perth_shortcodes.php
	===================================
	SHORTCODES
	===================================
*/
namespace Perth_theme\Shortcodes;

use Perth_theme\Perth_theme;

class Shortcodes extends Perth_theme {
	
	public function __construct() {
		$this->init();
		$this->activate_hook_callbacks();
	}

	public function init() {}

	public function activate_hook_callbacks() {

		add_shortcode('icon', array($this, 'perth_icon'));

	}

	public function perth_icon($atts, $content  = null) {
		$atts = shortcode_atts(array(
			'class'			=> ''
		),
		$atts,
        'icon');
        
        if ($atts['class'] != '') {
            return '<span class="icon"><i class="' . $atts['class'] .'"></i></span>';
        }
        return false;
        
		// return '<a href="' . $atts['href'] . '" class="' . $atts['class'] . '" data-inquiry="' . $inquiry . '">' . $value . '</a>';

	}
}

$shortcodes = new Shortcodes();