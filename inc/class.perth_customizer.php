<?php 
/**
	@package perth-project-theme
	class.perth_customizer.php
	===================================
	BASE CLASS
	===================================
*/
namespace Perth_theme\Customizer;

use Perth_theme\Control\Text_Editor_Custom_Control;
use Perth_theme\Control\Icon_Selector_Custom_Control;

use Perth_theme\Perth_theme;

/**
 * 
 */
class Customizer extends Perth_theme {
	
	public function __construct() {
		$this->init();
		$this->activate_hook_callbacks();
	}

	public function init() {

	}

	public function activate_hook_callbacks() {
		add_action('customize_preview_init', array($this, 'perth_customizer_live_preview'));
		// add_action('customize_register', array($this, 'perth_customizer_siteIdentity_content'));
		add_action('customize_register', array($this, 'perth_customizer_homepage_content'));
		add_action('customize_register', array($this, 'perth_customizer_aboutme_content'));
		add_action('customize_register', array($this, 'perth_customizer_portfolio_content'));
		add_action('customize_register', array($this, 'perth_customizer_donate_content'));
		add_action('customize_register', array($this, 'perth_customizer_contact_content'));
		add_action('customize_register', array($this, 'perth_customizer_footer_content'));
		// add_action('customize_register', array($this, 'perth_customizer_contact_content'));
	}

	public function perth_customizer_live_preview() {
		wp_enqueue_script('perth-admin-js', get_template_directory_uri() . '/assets/js/customizer.js', array(), false, true);
	}

	public function homepageDefaults($section) {
		$defaults = array(
			'header_section'	=> array(
				'title'		=> 'Meeth Perth,',
				'sub_title' => 'A stupidly simple, flat PSD. Oh yeah, it\'s <strong><i>free</i></strong> too!',
				'cta_link'	=> '#',
				'cta_text'	=> 'DOWNLOAD',
				'cta_icon'	=> 'fas fa-cloud-download-alt',
				'image'		=> get_template_directory_uri() . '/assets/img/iphone5.png'
			),
			'aboutme_section'	=> array(
				'title'		=> 'A Longwinded Header',
				'body'		=> '<p>She\'ll be right paddock how lets throw a ciggies. She\'ll be right ute to built like a doovalacky. We\'re going rack off bloody she\'ll be right pash. You little ripper tucker also get a dog up ya greenie.</p>

							<blockquote>You little ripper boozer no worries she\'ll be right holy dooley!. Come a freo also as cross as a your shout. Lets throw a khe sanh flamin dead dingo\'s donger. Lets get some fisho with grab us a trackie dacks.</blockquote>',
				'cta_link'	=> '#',
				'cta_text'	=> 'SAY HELLO',
				'cta_icon'	=> 'fas fa-envelope',
				'image'		=> get_template_directory_uri() . '/assets/img/ipad.png'
			),
			'portfolio_section' => array(
				'title'		=> 'Get a Dog Up Ya\'',
				'body'		=> '<p>
								<strong>She\'ll be right paddock how lets throw a ciggies. She\'ll be right ute to built like a doovalacky.</strong><br /><br />
								
								We\'re going rack off bloody she\'ll be right pash. You little ripper tucker also get a dog up ya greenie.
							</p>',
				'cta_link'	=> '#',
				'cta_text'	=> 'VIEW ON DRIBBLE',
				'cta_icon'	=> 'fa-basketball-ball',
				'image'		=> get_template_directory_uri() . '/assets/img/devices.png'
			),
			'donate_section'	=> array(
				'title'		=> 'Like What You See?',
				'cta_link'	=> '#',
				'cta_text'	=> 'BUY ME A <strong><i>BEER?</i>',
				'cta_icon'	=> 'fas fa-beer',
			),
			'contact_section'	=> array(
				'link1'		=> '#',
				'icon1'		=> 'fas fa-basketball-ball',
				'link2'		=> '#',
				'icon2'		=> 'fab fa-facebook-f',
				'link3'		=> '#',
				'icon3'		=> 'fab fa-twitter',
				'link4'		=> '#',
				'icon4'		=> 'fas fa-paper-plane'
			),
			'footer_section'=> array(
				'content'		=> '<p>&copy; <strong>2013</strong> <span>Perth.</span> <i>Breeding pixels since the naughties.</i></p>'
			)	
		);

		return $defaults[$section];
	}

	public function getThemeOptions($section) {
		$defaults = $this->homepageDefaults($section);
		$options = get_theme_mod($section, $defaults);

		foreach ($defaults as $key => $value) {
			if (!array_key_exists($key, $options) || $options[$key] == '' || $options[$key] == NULL) {
				$options[$key] = $value;
			}
		}

		return $options;

	}

	public function perth_customizer_homepage_content($wp_customize) {

		$defaults = $this->homepageDefaults('header_section');

		$wp_customize->add_panel('perth_homepage_content', array(
			'priority' => 5,
			'capability'=> 'edit_theme_options',
			'title'	=> 'Homepage Content',
			'description' => 'Edit Content of Home Page'
		));

		$wp_customize->add_section('header_content', array(
		    'priority'       => 0,
		    'capability'     => 'edit_theme_options',
		    'theme_supports' => '',
		    'title'          => __('Header Section', 'perth-project-theme'),
		    'description'    =>  __('Header Section Content', 'perth-project-theme'),
		    'panel'  => 'perth_homepage_content',
		));

		$wp_customize->add_setting('header_section[title]', array(
		    'default' => $defaults['title'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_homepageTitle_control', array(
		    'label' => 'Header Text',
		    'type' => 'text',
		    'section' => 'header_content',
		    'settings' => 'header_section[title]'
		));

		$wp_customize->selective_refresh->add_partial('header_section[title]', array(
			'selector'	=> '.hero-section h1.title',
			'render_callback' => function () {
				echo get_theme_mod('header_section')['title'];
			}
		));

		$wp_customize->add_setting('header_section[sub_title]', array(
		    'default' => $defaults['sub_title'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_homepageSubtitle_control', array(
		    'label' => 'Sub Header Text',
		    'type' => 'textarea',
		    'section' => 'header_content',
		    'settings' => 'header_section[sub_title]'
		));

		$wp_customize->selective_refresh->add_partial('header_section[sub_title]', array(
			'selector'	=> '.hero-section h3.sub-title',
			'render_callback' => function () {
				echo get_theme_mod('header_section')['sub_title'];
			}
		));

		$wp_customize->add_setting('header_section[cta_link]', array(
		    'default' => $defaults['cta_link'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_homepageCtalink_control', array(
		    'label' => 'CTA Link',
		    'type' => 'url',
		    'section' => 'header_content',
		    'settings' => 'header_section[cta_link]'
		));

		$wp_customize->add_setting('header_section[cta_text]', array(
		    'default' => $defaults['cta_text'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_homepageCtatext_control', array(
		    'label' => 'CTA Text',
		    'type' => 'text',
		    'section' => 'header_content',
		    'settings' => 'header_section[cta_text]'
		));

		$wp_customize->selective_refresh->add_partial('header_section[cta_text]', array(
			'selector'	=> '.hero-section .body a.btn span',
			'render_callback' => function () {
				echo get_theme_mod('header_section')['cta_text'];
			}
		));

		$wp_customize->add_setting('header_section[cta_icon]', array(
		    'default' => $defaults['cta_icon'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new Icon_Selector_Custom_Control($wp_customize, 'header_section[cta_icon]', array(
			'label' => __('CTA Icon', 'perth-project-theme'),
			'description' => __('Choose an icon', 'perth-project-theme'),
			'iconset' => 'fa',
			'section'	=> 'header_content',
			'settings'	=> 'header_section[cta_icon]'
		)));

		$wp_customize->add_setting('header_section[image]', array(
			'default'	=> $defaults['image'],
			'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new \WP_Customize_Image_Control(
           $wp_customize,
           'header_section[image]',
           array(
               'label'      => __('Upload a Header Image', 'perth-project-theme'),
               'section'    => 'header_content',
               'settings'   => 'header_section[image]'
           )
       ));

		$wp_customize->selective_refresh->add_partial('header_section[image]', array(
			'selector'	=> 'section.hero-section .footer img',
			'render_callback' => function () {
				echo get_theme_mod('header_section')['image'];
			}
		));

	}

	public function perth_customizer_aboutme_content($wp_customize) {

		$defaults = $this->homepageDefaults('aboutme_section');

		$wp_customize->add_section('aboutme_content', array(
		    'priority'       => 1,
		    'capability'     => 'edit_theme_options',
		    'theme_supports' => '',
		    'title'          => __('About Me Section', 'perth-project-theme'),
		    'description'    =>  __('About Me Section Content', 'perth-project-theme'),
		    'panel'  => 'perth_homepage_content',
		));

		$wp_customize->add_setting('aboutme_section[title]', array(
		    'default' => $defaults['title'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_aboutmeTitle_control', array(
		    'label' => 'Header Text',
		    'type' => 'text',
		    'section' => 'aboutme_content',
		    'settings' => 'aboutme_section[title]'
		));

		$wp_customize->selective_refresh->add_partial('aboutme_section[title]', array(
			'selector'	=> '.about-me .head h1.title',
			'render_callback' => function () {
				echo get_theme_mod('aboutme_section')['title'];
			}
		));

		$wp_customize->add_setting('aboutme_section[body]', array(
		    'default' => $defaults['body'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new Text_Editor_Custom_Control($wp_customize, 'aboutme_section[body]', array(
			'label' => 'Body',
		    'section' => 'aboutme_content',
		    'settings' => 'aboutme_section[body]'
		)));

		$wp_customize->selective_refresh->add_partial('aboutme_section[body]', array(
			'selector'	=> '.about-me .body div.content',
			'render_callback' => function () {
				echo get_theme_mod('aboutme_section')['body'];
			}
		));

		$wp_customize->add_setting('aboutme_section[cta_link]', array(
		    'default' => $defaults['cta_link'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_aboutmeCtalink_control', array(
		    'label' => 'CTA Link',
		    'type' => 'url',
		    'section' => 'aboutme_content',
		    'settings' => 'aboutme_section[cta_link]'
		));

		$wp_customize->add_setting('aboutme_section[cta_text]', array(
		    'default' => $defaults['cta_text'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_aboutmeCtatext_control', array(
		    'label' => 'CTA Text',
		    'type' => 'text',
		    'section' => 'aboutme_content',
		    'settings' => 'aboutme_section[cta_text]'
		));

		$wp_customize->selective_refresh->add_partial('aboutme_section[cta_text]', array(
			'selector'	=> '.about-me .body a.btn span',
			'render_callback' => function () {
				echo get_theme_mod('aboutme_section')['cta_text'];
			}
		));
		

		$wp_customize->add_setting('aboutme_section[cta_icon]', array(
		    'default' => $defaults['cta_icon'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new Icon_Selector_Custom_Control($wp_customize, 'aboutme_section[cta_icon]', array(
			'label' => __('CTA Icon', 'perth-project-theme'),
			'description' => __('Choose an icon', 'perth-project-theme'),
			'iconset' => 'fa',
			'section'	=> 'aboutme_content',
			'settings'	=> 'aboutme_section[cta_icon]'
		)));

		$wp_customize->add_setting('aboutme_section[image]', array(
			'default'	=> $defaults['image'],
			'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new \WP_Customize_Image_Control(
           $wp_customize,
           'aboutme_section[image]',
           array(
               'label'      => __('Upload a Header Image', 'perth-project-theme'),
               'section'    => 'aboutme_content',
               'settings'   => 'aboutme_section[image]'
           )
       ));

		$wp_customize->selective_refresh->add_partial('aboutme_section[image]', array(
			'selector'	=> 'section.about-me .footer img',
			'render_callback' => function () {
				echo get_theme_mod('aboutme_section')['image'];
			}
		));

	}

	public function perth_customizer_portfolio_content($wp_customize) {
		$defaults = $this->homepageDefaults('portfolio_section');

		$wp_customize->add_section('portfolio_content', array(
		    'priority'       => 2,
		    'capability'     => 'edit_theme_options',
		    'theme_supports' => '',
		    'title'          => __('Portfolio Section', 'perth-project-theme'),
		    'description'    =>  __('Portfolio Section Content', 'perth-project-theme'),
		    'panel'  => 'perth_homepage_content',
		));

		$wp_customize->add_setting('portfolio_section[title]', array(
		    'default' => $defaults['title'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_portfolioTitle_control', array(
		    'label' => 'Header Text',
		    'type' => 'text',
		    'section' => 'portfolio_content',
		    'settings' => 'portfolio_section[title]'
		));

		$wp_customize->selective_refresh->add_partial('portfolio_section[title]', array(
			'selector'	=> 'section.portfolio .head h1.title',
			'render_callback' => function () {
				echo get_theme_mod('portfolio_section')['title'];
			}
		));

		$wp_customize->add_setting('portfolio_section[body]', array(
		    'default' => $defaults['body'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new Text_Editor_Custom_Control($wp_customize, 'portfolio_section[body]', array(
			'label' => 'Body',
		    'section' => 'portfolio_content',
		    'settings' => 'portfolio_section[body]'
		)));

		$wp_customize->selective_refresh->add_partial('portfolio_section[body]', array(
			'selector'	=> 'section.portfolio .body div.content',
			'render_callback' => function () {
				echo get_theme_mod('portfolio_section')['body'];
			}
		));

		$wp_customize->add_setting('portfolio_section[cta_link]', array(
		    'default' => $defaults['cta_link'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_portfolioCtalink_control', array(
		    'label' => 'CTA Link',
		    'type' => 'url',
		    'section' => 'portfolio_content',
		    'settings' => 'portfolio_section[cta_link]'
		));

		$wp_customize->add_setting('portfolio_section[cta_text]', array(
		    'default' => $defaults['cta_text'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_ctatext_control', array(
		    'label' => 'CTA Text',
		    'type' => 'text',
		    'section' => 'portfolio_content',
		    'settings' => 'portfolio_section[cta_text]'
		));

		$wp_customize->selective_refresh->add_partial('portfolio_section[cta_text]', array(
			'selector'	=> 'section.portfolio .body a.btn span',
			'render_callback' => function () {
				echo get_theme_mod('portfolio_section')['cta_text'];
			}
		));

		$wp_customize->add_setting('portfolio_section[cta_icon]', array(
		    'default' => $defaults['cta_icon'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new Icon_Selector_Custom_Control($wp_customize, 'portfolio_section[cta_icon]', array(
			'label' => __('CTA Icon', 'perth-project-theme'),
			'description' => __('Choose an icon', 'perth-project-theme'),
			'iconset' => 'fa',
			'section'	=> 'portfolio_content',
			'settings'	=> 'portfolio_section[cta_icon]'
		)));

		$wp_customize->add_setting('portfolio_section[image]', array(
			'default'	=> $defaults['image'],
			'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new \WP_Customize_Image_Control(
           $wp_customize,
           'portfolio_section[image]',
           array(
               'label'      => __('Upload a Header Image', 'perth-project-theme'),
               'section'    => 'portfolio_content',
               'settings'   => 'portfolio_section[image]'
           )
       ));

		$wp_customize->selective_refresh->add_partial('portfolio_section[image]', array(
			'selector'	=> 'section.portfolio .footer img',
			'render_callback' => function () {
				echo get_theme_mod('portfolio_section')['image'];
			}
		));
	}

	public function perth_customizer_donate_content($wp_customize) {
		$defaults = $this->homepageDefaults('donate_section');

		$wp_customize->add_section('donate_content', array(
		    'priority'       => 3,
		    'capability'     => 'edit_theme_options',
		    'theme_supports' => '',
		    'title'          => __('Donate Section', 'perth-project-theme'),
		    'description'    =>  __('Donate Section Content', 'perth-project-theme'),
		    'panel'  => 'perth_homepage_content',
		));

		$wp_customize->add_setting('donate_section[title]', array(
		    'default' => $defaults['title'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_donateTitle_control', array(
		    'label' => 'Header Text',
		    'type' => 'text',
		    'section' => 'donate_content',
		    'settings' => 'donate_section[title]'
		));

		$wp_customize->selective_refresh->add_partial('donate_section[title]', array(
			'selector'	=> 'section.donate .head h1.title',
			'render_callback' => function () {
				echo get_theme_mod('donate_section')['title'];
			}
		));

		$wp_customize->add_setting('donate_section[cta_link]', array(
		    'default' => $defaults['cta_link'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_donateCtalink_control', array(
		    'label' => 'CTA Link',
		    'type' => 'url',
		    'section' => 'donate_content',
		    'settings' => 'donate_section[cta_link]'
		));

		$wp_customize->add_setting('donate_section[cta_text]', array(
		    'default' => $defaults['cta_text'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_donateCtatext_control', array(
		    'label' => 'CTA Text',
		    'type' => 'text',
		    'section' => 'donate_content',
		    'settings' => 'donate_section[cta_text]'
		));

		$wp_customize->selective_refresh->add_partial('donate_section[cta_text]', array(
			'selector'	=> 'section.donate .body a.btn span',
			'render_callback' => function () {
				echo get_theme_mod('donate_section')['cta_text'];
			}
		));

		$wp_customize->add_setting('donate_section[cta_icon]', array(
		    'default' => $defaults['cta_icon'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new Icon_Selector_Custom_Control($wp_customize, 'donate_section[cta_icon]', array(
			'label' => __('CTA Icon', 'perth-project-theme'),
			'description' => __('Choose an icon', 'perth-project-theme'),
			'iconset' => 'fa',
			'section'	=> 'donate_content',
			'settings'	=> 'donate_section[cta_icon]'
		)));
	}

	public function perth_customizer_contact_content($wp_customize) {

		$defaults = $this->homepageDefaults('contact_section');

		$wp_customize->add_section('contact_content', array(
			'priority'		=> 4,
			'capability'	=> 'edit_theme_options',
			'theme_supports'=> '',
			'title'			=> __('Contact Section', 'perth-project-theme'),
			'description'	=> __('Contact Content', 'perth-project-theme'),
			'panel'			=> 'perth_homepage_content'
		));

		$wp_customize->add_setting('contact_section[link1]', array(
		    'default' => $defaults['link1'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_contactLink1_control', array(
		    'label' => 'Link 1',
		    'type' => 'url',
		    'section' => 'contact_content',
		    'settings' => 'contact_section[link1]'
		));

		$wp_customize->add_setting('contact_section[icon1]', array(
			'default'		=> $defaults['icon1'],
			'transport'		=> 'postMessage'
		));

		$wp_customize->add_control(new Icon_Selector_Custom_Control($wp_customize, 'contanct_section[icon1]', array(
			'label'			=> 'Icon 1',
			'section'		=> 'contact_content',
			'settings'		=> 'contact_section[icon1]'
		)));

		$wp_customize->selective_refresh->add_partial('contact_section[icon1]', array(
			'selector'	=> 'section.contact .body .social-media a#link1',
			'render_callback' => function () {
				echo '<i class="' . get_theme_mod('contact_section')['icon1'] . '"></i>';
			}
		));

		// link 2
		$wp_customize->add_setting('contact_section[link2]', array(
		    'default' => $defaults['link2'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_contactLink2_control', array(
		    'label' => 'Link 2',
		    'type' => 'url',
		    'section' => 'contact_content',
		    'settings' => 'contact_section[link2]'
		));

		$wp_customize->add_setting('contact_section[icon2]', array(
			'default'		=> $defaults['icon2'],
			'transport'		=> 'postMessage'
		));

		$wp_customize->add_control(new Icon_Selector_Custom_Control($wp_customize, 'contanct_section[icon2]', array(
			'label'			=> 'Icon 2',
			'section'		=> 'contact_content',
			'settings'		=> 'contact_section[icon2]'
		)));

		$wp_customize->selective_refresh->add_partial('contact_section[icon2]', array(
			'selector'	=> 'section.contact .body .social-media a#link2',
			'render_callback' => function () {
				echo '<i class="' . get_theme_mod('contact_section')['icon2'] . '"></i>';
			}
		));

		// Link 3
		$wp_customize->add_setting('contact_section[link3]', array(
		    'default' => $defaults['link3'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_contactLink3_control', array(
		    'label' => 'Link 3',
		    'type' => 'url',
		    'section' => 'contact_content',
		    'settings' => 'contact_section[link3]'
		));

		$wp_customize->add_setting('contact_section[icon3]', array(
			'default'		=> $defaults['icon3'],
			'transport'		=> 'postMessage'
		));

		$wp_customize->add_control(new Icon_Selector_Custom_Control($wp_customize, 'contact_section[icon3]', array(
			'label'			=> 'Icon 3',
			'section'		=> 'contact_content',
			'settings'		=> 'contact_section[icon3]'
		)));

		$wp_customize->selective_refresh->add_partial('contact_section[icon3]', array(
			'selector'	=> 'section.contact .body .social-media a#link3',
			'render_callback' => function () {
				echo '<i class="' . get_theme_mod('contact_section')['icon3'] . '"></i>';
			}
		));

		// Link 4
		$wp_customize->add_setting('contact_section[link4]', array(
		    'default' => $defaults['link4'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control('header_contactLink4_control', array(
		    'label' => 'Link 4',
		    'type' => 'url',
		    'section' => 'contact_content',
		    'settings' => 'contact_section[link4]'
		));

		$wp_customize->add_setting('contact_section[icon4]', array(
			'default'		=> $defaults['icon4'],
			'transport'		=> 'postMessage'
		));

		$wp_customize->add_control(new Icon_Selector_Custom_Control($wp_customize, 'contanct_section[icon3]', array(
			'label'			=> 'Icon 4',
			'section'		=> 'contact_content',
			'settings'		=> 'contact_section[icon4]'
		)));

		$wp_customize->selective_refresh->add_partial('contact_section[icon4]', array(
			'selector'	=> 'section.contact .body .social-media a#link4',
			'render_callback' => function () {
				echo '<i class="' . get_theme_mod('contact_section')['icon4'] . '"></i>';
			}
		));
	}

	public function perth_customizer_footer_content($wp_customize) {
		$defaults = $this->homepageDefaults('footer_section');

		$wp_customize->add_section('footer_content', array(
		    'priority'       => 5,
		    'capability'     => 'edit_theme_options',
		    'theme_supports' => '',
		    'title'          => __('Footer Section', 'perth-project-theme'),
		    'description'    =>  __('Footer Content', 'perth-project-theme'),
		    'panel'  => 'perth_homepage_content',
		));

		$wp_customize->add_setting('footer_section[content]', array(
		    'default' => $defaults['content'],
		    'transport'	=> 'postMessage'
		));

		$wp_customize->add_control(new Text_Editor_Custom_Control($wp_customize, 'footer_section[content]', array(
			'label' => 'Content',
		    'section' => 'footer_content',
		    'settings' => 'footer_section[content]'
		)));


		$wp_customize->selective_refresh->add_partial('footer_section[content]', array(
			'selector'	=> 'footer div.content',
			'render_callback' => function () {
				echo get_theme_mod('footer_section')['content'];
			}
		));

	}

}

$customizer = new Customizer();