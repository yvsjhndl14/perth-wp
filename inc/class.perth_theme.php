<?php 
/**
	@package perth-project-theme
	class.perth_theme.php
	===================================
	BASE CLASS
	===================================
*/
namespace Perth_theme;

abstract class Perth_theme {
	
	abstract protected function __construct();

	// abstract public function activate($network_wide);

	// public function __get($variable) {
	// 	$module = get_called_class();
		// $class = new $module;
		// return $class->$variable;

		// if (in_array($variable, $module::$readable_properties))
		// 	return $this->variable;
		// else
		// 	throw new Exception( __METHOD__ . " error: $". $variable ." doesn't exist or isn't readable." );
	// }

	abstract public function init();

	abstract public function activate_hook_callbacks();

	// abstract public function upgrade($db_version = 0);
}