<?php
/**
	@package perth-project-theme
	class.perth_custom_function.php
	===================================
	CUSTOM FUNCTIONS
	===================================
*/
namespace Perth_theme\Functions;

use Perth_theme\Perth_theme;

class Func extends Perth_theme {
	
	public function __construct() {
		$this->init();
		$this->activate_hook_callbacks();
	}

	public function init() {}

	public function activate_hook_callbacks() {

        add_filter('wp_nav_menu_objects', array($this, 'generate_navigation_shortcode'));
    }

    public function generate_navigation_shortcode($menu_items) {

        foreach ($menu_items as $menu_item) {
            $menu_item->title = do_shortcode($menu_item->title);
        }

        return $menu_items;
    }
}

$functions = new Func();