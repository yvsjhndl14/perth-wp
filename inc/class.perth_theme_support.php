<?php 
/**
	@package perth-project-theme
	class.perth_theme_support.php
	===================================
	THEME SUPPORTS ACTIVATION
	===================================
*/
namespace Perth_theme\Theme_support;

use Perth_theme\Perth_theme;

class Theme_support extends Perth_theme {
	
	private $html5 = array();
	private $post_formats = array();

	public function __construct() {
		$this->init();
		$this->activate_hook_callbacks();
	}

	public function init() {
		// $this->html5 = array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption');
		// $this->post_formats = array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat');
	}

	public function activate_hook_callbacks() {

		// add_theme_support('html5', $this->html5);
		// add_theme_support('post-formats', $this->post_formats);
		// add_theme_support('post-thumbnails');
		// add_theme_support('custom-header');


		add_theme_support('customize-selective-refresh-widgets');

		add_post_type_support('page', 'excerpt');
	}
}

$theme_support = new Theme_support();