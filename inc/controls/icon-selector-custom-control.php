<?php
/**
	@package perth-project-theme
    icon-selector-custom-control.php
	===================================
	ICON SELECTOR CUSTOM CONTROL
	===================================
*/
namespace Perth_theme\Control;

if ( ! class_exists( 'WP_Customize_Control' ) )
    return NULL;

/**
 * Class to create a custom tags control
 */
class Icon_Selector_Custom_Control extends \WP_Customize_Control
{   
    public $choices = array();

    public $type = 'o2-icon-picker';
    
    public function enqueue() {
        wp_enqueue_script( 'o2-icon-picker-ddslick', get_template_directory_uri() . '/assets/js/jquery.ddslick.js', array( 'jquery' ) );
        wp_enqueue_script( 'o2-icon-picker-control', get_template_directory_uri() . '/assets/js/icon-picker-control.js', array( 'jquery', 'o2-icon-picker-ddslick' ), '', true );

        wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.4.1/css/all.css');
    }

    /**
       * Render the content on the theme customizer page
    */
    public function render_content() {

        require_once get_template_directory() . '/inc/controls/icons/fa-icons.php';
        require_once get_template_directory() . '/inc/controls/icons/fab-icons.php';
        $fa = fa_font_awesome_list();
        $fab = fab_font_awesome_brand_list();
        
        $this->choices = array_merge($fab, $fa);

        ?>

        <label>
            <?php if ( ! empty( $this->label ) ) : ?>
                <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
            <?php endif;
            if ( ! empty( $this->description ) ) : ?>
                <span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
            <?php endif; ?>
            <?php $text_id = str_replace(array('[', ']'), '_', $this->id); ?>
            <select class="o2-icon-picker-icon-control" data-customize-setting-link="<?php echo esc_attr( $this->id ); ?>" id="<?php echo esc_attr($text_id); // esc_attr( $this->id ); ?>">
                <?php foreach ( $this->choices as $value => $label ) : ?>
                    <option value="<?php echo esc_attr( $value ); ?>" <?php echo selected( $this->value(), $value, false ); ?> data-iconsrc="<?php echo esc_attr( $value ); ?>"><?php // echo esc_html('<i class="fas ' . $value .'"></i>'); ?><?php echo esc_html( $label ); ?></option>
                <?php endforeach; ?>
            </select>
        </label>

        <?php
    }
}