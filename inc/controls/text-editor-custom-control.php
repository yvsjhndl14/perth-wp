<?php
/**
  @package perth-project-theme
  text-editor-custom-control.php
	===================================
	TEXT EDITOR CUSTOM CONTROL
	===================================
*/
namespace Perth_theme\Control;

if ( ! class_exists( 'WP_Customize_Control' ) )
    return NULL;
/**
 * Class to create a custom tags control
 */
class Text_Editor_Custom_Control extends \WP_Customize_Control
{
      /**
       * Render the content on the theme customizer page
       */
      public function render_content()
       {
          $text_id = str_replace(array('[', ']'), '_', $this->id);
            ?>
                <label>
                  <span class="customize-text_editor"><?php echo esc_html( $this->label ); ?></span>
                  <input id="<?php echo $this->id ?>-link" class="wp-editor-area" type="hidden" <?php $this->link(); ?> value="<?php echo esc_textarea($value); ?>">
                  <?php

                    $settings = array(
                      'textarea_name' => $text_id,
                      'media_buttons' => false,
                      'tinymce' => array(
                        'setup' => "function (editor) {
                            var cb = function () {
                              var linkInput = document.getElementById('$this->id-link')
                              linkInput.value = editor.getContent()
                              linkInput.dispatchEvent(new Event('change'))
                            }
                            editor.on('Change', cb)
                            editor.on('Undo', cb)
                            editor.on('Redo', cb)
                            editor.on('KeyUp', cb) // Remove this if it seems like an overkill
                          }"
                      )
                    );
                    wp_editor($this->value(), $text_id, $settings);
                    do_action('admin_print_footer_scripts');
                  ?>
                </label>
            <?php
       }
}