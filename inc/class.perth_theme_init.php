<?php 
/**
	@package perth-project-theme
	class.perth_theme_init.php
	===================================
	INITIALIZE THEME OPTIONS
	===================================
*/
namespace Perth_theme\Init;

use Perth_theme\Perth_theme;

class Init extends Perth_theme {

	private $directory;

	public $post_thumbnails = array();

	public function __construct() {
		$this->init();
		$this->activate_hook_callbacks();
	}

	public function init() {
		$this->directory = get_template_directory_uri();
	}

	public function activate_hook_callbacks() {
		add_filter('script_loader_src', array($this, 'perth_remove_wp_version_strings'));
		add_filter('style_loader_src', array($this, 'perth_remove_wp_version_strings'));

		add_filter('the_generator', array($this, 'perth_remove_meta_version'));

		add_action('wp_enqueue_scripts', array($this, 'perth_enqueue_public_scripts'));
		add_action('admin_enqueue_scripts', array($this, 'perth_enqueue_admin_scripts'));

		add_action('after_setup_theme', array($this, 'perth_register_nav_menu'));

		add_action('widgets_init', array($this, 'perth_register_sidebar'));

		remove_filter('term_description', 'wpautop');
	}

	/**
	 * Remove version strig from js and css
	 *
	 * @return string
	*/
	public function perth_remove_wp_version_strings($src) {
		global $wp_version;
		parse_str(parse_url($src, PHP_URL_QUERY), $query);
		if (!empty($query['ver']) && $query['ver'] === $wp_version) {
			$src = remove_query_arg('ver', $src);
		}
		return $src;
	}

	/**
	 * Remove meta tag generator header
	 *
	 * @return string
	*/
	public function perth_remove_meta_version() {
		return '';
	}

	/**
	 * Enqueue public styles and scripts
	 *
	 * @return string
	*/
	public function perth_enqueue_public_scripts() {

		// Enqueue styles
		wp_enqueue_style('perth-project-css', get_template_directory_uri() . '/assets/css/style.min.css', array(), '0.1.0', 'all');
		wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.4.1/css/all.css');

		// Enqueue scripts
		wp_enqueue_script('perth-project-js', get_template_directory_uri() . '/assets/js/script.min.js', array(), false, true);

	}

	/**
	 * Enqueue admin styles and scripts
	 *
	 * @return string
	*/
	public function perth_enqueue_admin_scripts() {
		// Enqueue styles
		wp_enqueue_style('perth-project-css', get_template_directory_uri() . '/assets/css/admin.css', array(), '0.1.0', 'all');

	}

	/**
	 * Register Nav menus
	*/
	public function perth_register_nav_menu() {
		register_nav_menu('primary', 'Header Navigation Menu');
	}

	/**
	 * Register Sidebars
	*/
	public function perth_register_sidebar() {

		$args = array(
			'name'          => __('Perth Homepage Widget', 'perth-project-theme'),
			'id'            => 'perth-homepage-widget',
			'description'   => 'Homepage widget',
			'class'         => '',
			'before_widget' => '<div id="%1s" class="%2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<label>',
			'after_title'   => '</label>',
		);
		
		register_sidebar($args);
	}

	public function perth_register_post_thumbnails() {

		if (class_exists('MultiPostThumbnails')) {
			foreach ($this->post_thumbnails as $key => $thumbnail) {
				foreach ($thumbnail['post_type'] as $post_type) {
					new \MultiPostThumbnails(array(
						'label'		=> $thumbnail['label'],
						'id'		=> $thumbnail['id'],
						'post_type' => $post_type
					));
				}
			}
		}

	}

	
}

//create new object 
$init = new Init();