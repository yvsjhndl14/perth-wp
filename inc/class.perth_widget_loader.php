<?php 
/**
	@package perth-project-theme
	class.perth_widget_loader.php
	===================================
	LOAD WIDGETS
	===================================
*/

namespace Perth_theme\Widget;

use Perth_theme\Perth_theme;

class Load_widgets extends Perth_theme {
	
	public function __construct() {
		$this->init();
		$this->activate_hook_callbacks();
	}

	public function init() {}

	public function activate_hook_callbacks() {
		add_action('widgets_init', array($this, 'register_widgets'));
	}

	public function register_widgets() {
		register_widget('Perth_theme\Widget\Newsletter');
	}
}

$load = new Load_widgets();