# Perth Test Project

Content Modification: Customize section

### Shortcode

Shortcode used in nav menu for menu icon

```
[icon class={font awesome class}]{title}
```

### Nav menus

Matches the given psd

* [icon class="fas fa-home"]home
* [icon class="far fa-heart"]about me
* [icon class="fas fa-desktop"]portfolio
* [icon class="far fa-envelope"]drop me a line

### Homepage widget

Visible on Contact Us Section.

```
Add the Newsletter widget
```