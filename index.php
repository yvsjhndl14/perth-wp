<?php 
/**
	@package subnet-construction-theme
	index.php
	===================================
	INDEX PAGE
	===================================
*/
?>
<?php get_header(); ?>
	
	<?php if (have_posts()): ?>

	<!-- Begin: page header -->
	<section id="page-title">
		<div class="container">
			<h2>Blog Posts</h2>
		</div>
	</section>
	<!-- End: page header -->

	<div id="" class="section"><!-- .section -->
		<div class="container"><!-- .container -->

			<div class="row"><!-- .row -->
				<div class="col s12 l8"><!-- .s12 -->
					<ul>
					<?php while (have_posts()): the_post(); ?>
						<li>
						<div class="card" style="margin-bottom: 50px;">
							
							<?php if (has_post_thumbnail()): ?>
							<a href="<?php the_permalink(); ?>">
								<div class="card-image"><!-- .card-image -->
									
									<img src="<?php echo esc_url(the_post_thumbnail_url('medium_large')); ?>" alt="<?php the_post_thumbnail_caption(); ?>">
									
								</div><!-- .card-image -->
							</a>
							<?php endif; ?>	
							
							<div class="card-content">
								<?php the_title('<span class="card-title">', '</span>'); ?>
								<?php the_excerpt(); ?>	
								
							</div>

							<div class="card-action">
								<a href="<?php the_permalink(); ?>" class="btn-large brand-green waves-effect waves-dark">Read More</a>
							</div>
						
						</div>
						</li>

					<?php endwhile; ?>
					</ul>
				</div><!-- .s12 -->


				<div class="col s12 l4">
					<?php if (is_active_sidebar('subcon-sidebar')) dynamic_sidebar('subcon-sidebar');  ?>
				</div>

			</div><!-- .row -->

			<!-- BEGIN: LOAD MORE BUTTON -->
			<div class="prop-footer">
		   		
			<?php $function->subcon_pagination(); ?>

		    </div>						   
		    <!-- END: LOAD MORE BUTTON -->

		</div><!-- .container -->
	</div><!-- .section -->

	<?php else: ?>

		<h2>No Post</h2>

	<?php endif; ?>

<?php get_footer(); ?>