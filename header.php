<?php
/**
	THEME HEADER

	@package subnet-construction-theme
*/
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equi="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title('-'); ?></title>
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<?php wp_head(); ?>
	</head>

<!-- Begin: body -->
<body <?php body_class(''); ?>>

	<!-- Begin: header -->
	<header>
		<div class="navbar-fixed"><!-- .navbar-fixed -->
			<nav role="navigation" class="white"><!-- .white -->
				<div class="nav-wrapper"><!-- .nav-wrapper -->

					<?php wp_nav_menu(array(
						'theme_location'		=> 'primary',
						'container'				=> false,
						'menu_class'			=> 'right hide-on-med-and-down',
						'walker'				=> new \Subcon_Theme\Walkers\Primary_nav()
					)); ?>

					<a href="#" data-activates="slide-out" class="button-collapse hamburger">
						<span></span>
						<span></span>
						<span></span>
					</a>

				</div><!-- .nav-wrapper -->
			</nav><!-- .white -->
		</div><!-- .navbar-fixed -->
	</header>
	<!-- End: header -->