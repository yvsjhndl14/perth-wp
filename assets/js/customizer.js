( function( $ ) {

    // Header Section
    wp.customize('header_section[cta_link]', function( setting ) {
        setting.bind( function( data ) {
            $('section.hero-section .body a.btn').attr('href', data);
        });
    });

    wp.customize('header_section[cta_icon]', function( setting ) {
        setting.bind( function( data ) {
            $('section.hero-section .body a.btn i').removeClass();
            $('section.hero-section .body a.btn i').attr('class', data);
        });
    });

    wp.customize( 'header_section[image]', function( setting ) {
        setting.bind( function( data ) {
            $('section.hero-section .footer img').attr('src', data);
        });
    });

    //  About me section
    wp.customize('aboutme_section[cta_link]', function( setting ) {
        setting.bind( function( data ) {
            $('section.about-me .body a.btn').attr('href', data);
        });
    });

    wp.customize('aboutme_section[cta_icon]', function( setting ) {
        setting.bind( function( data ) {
            $('section.about-me .body a.btn i').removeClass().attr('class', data);
        });
    });

    wp.customize( 'aboutme_section[image]', function( setting ) {
        setting.bind( function( data ) {
            $('section.about-me .footer img').attr('src', data);
        });
    });

    //  Portfolio section
    wp.customize('portfolio_section[cta_link]', function( setting ) {
        setting.bind( function( data ) {
            $('section.portfolio .body a.btn').attr('href', data);
        });
    });

    wp.customize('portfolio_section[cta_icon]', function( setting ) {
        setting.bind( function( data ) {
            $('section.portfolio .body a.btn i').removeClass().attr('class', data);
        });
    });

    wp.customize( 'portfolio_section[image]', function( setting ) {
        setting.bind( function( data ) {
            $('section.portfolio .footer img').attr('src', data);
        });
    });

    //  Donate Section
    wp.customize('donate_section[cta_link]', function( setting ) {
        setting.bind( function( data ) {
            $('section.donate .body a.btn').attr('href', data);
        });
    });

    wp.customize('donate_section[cta_icon]', function( setting ) {
        setting.bind( function( data ) {
            $('section.donate .body a.btn i#icon').removeClass().attr('class', data);
        });
    });


    //  Contact Section
    // wp.customize.section('contact_content')
    wp.customize('contact_section[link1]', function( setting ) {
        setting.bind( function( data ) {
            $('section.contact .body .social-media a#link1').attr('href', data);
        });
    });

    // wp.customize('contact_section[icon1]', function( setting ) {
    //     setting.bind( function( data ) {
    //         $('section.contact .body .social-media a#link1 i').removeClass().attr('class', ' ' + data);
    //     });
    // });

    wp.customize('contact_section[link2]', function( setting ) {
        setting.bind( function( data ) {
            $('section.contact .body .social-media a#link2').attr('href', data);
        });
    });

    // wp.customize('contact_section[icon2]', function( setting ) {
    //     setting.bind( function( data ) {
    //         $('section.contact .body .social-media a#link2 i').removeClass().attr('class', ' ' + data);
    //     });
    // });

    wp.customize('contact_section[link3]', function( setting ) {
        setting.bind( function( data ) {
            $('section.contact .body .social-media a#link3').attr('href', data);
        });
    });

    // wp.customize('contact_section[icon3]', function( setting ) {
    //     setting.bind( function( data ) {
    //         $('section.contact .body .social-media a#link3 i').removeClass().attr('class', ' ' + data);
    //     });
    // });

    wp.customize('contact_section[link4]', function( setting ) {
        setting.bind( function( data ) {
            $('section.contact .body .social-media a#link4').attr('href', data);
        });
    });

    // wp.customize('contact_section[icon4]', function( setting ) {
    //     setting.bind( function( data ) {
    //         $('section.contact .body a#link4 i').removeClass().attr('class', ' ' + data);
    //     });
    // });


} )( jQuery );