<?php 
/**
	@package subnet-construction-theme
	footer.php
	===================================
	FOOTER
	===================================
*/
?>
<!-- Begin: footer -->
<footer class="page-footer">
	<div class="container"><!-- .container -->

		<div class="row"><!-- .row -->

		<?php if (is_active_sidebar('subcon-footerbar')) dynamic_sidebar('subcon-footerbar');  ?>

		</div><!-- .row -->
	</div><!-- .container -->

	<div class="footer-copyright"><!-- .footer-copyright -->
		<div class="container"><!-- .container -->
			<!-- footer customizer -->
			<div class="right footer-links"><!-- .footer-links -->
			
			</div><!-- .footer-links -->
		</div><!-- .container -->
	</div><!-- .footer-copyright -->

</footer>
<!-- End: footer -->

<?php wp_footer(); ?>

</body>
<!-- End: body -->
</html>