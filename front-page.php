<?php
/**
	THEME HEADER

	@package perth-project-theme
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<?php wp_head(); ?>
</head>
<body>
	<?php if (have_posts()): ?>
		<?php while (have_posts()): the_post(); ?>

		<!-- Home Header -->
		<!-- END Home Header -->
		<!-- Home Main -->
		<main role="main" class="container-fluid">	
			<!-- .hero-section -->
			<?php 
				$header_section = $customizer->getThemeOptions('header_section');
			?>
			<section class="row hero-section">
				<div id="nav-overlay" class="nav-overlay">
					<?php wp_nav_menu(array(
						'theme_location'  => 'primary',
						'menu'            => '',
						'container'       => 'div',
						'container_class' => 'menu-{menu-slug}-container',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					)); ?>
				</div>
				
				<div class="content col-md-12 col-lg-8 col-lg-offset-2">
					<div class="head">
						<div class="logo">
							<span>p</span>
						</div>
						<div id="nav-button" class="toggle-nav">
							<div class="burger-menu">
							</div>
						</div>	
					</div>
					<div class="body">
						<h1 class="title"><?php echo $header_section['title']; ?></h1>
						<h3 class="sub-title"><?php echo $header_section['sub_title']; ?></h3>

						<a href="<?php echo $header_section['cta_link'] ?>" class="btn btn-default btn-outlined"><span><?php echo $header_section['cta_text'] ?></span> <i class="<?php echo $header_section['cta_icon'] ?>"></i></a>
					</div>
					<div class="footer">
						<img src="<?php echo $header_section['image']; ?>">
					</div>
				</div>
			</section>
			<!-- END: .hero-section -->

			<?php 
				$aboutme_section = $customizer->getThemeOptions('aboutme_section');
			?>
			<!-- .about-me -->
			<section class="row about-me">
				<div class="col-md-12 col-lg-8 col-lg-offset-2">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="head">
								<h1 class="title"><?php echo $aboutme_section['title']; ?><!-- A Longwinded Header --></h1>
							</div>
							<div class="body">
								<div class="content">
									<?php echo $aboutme_section['body']; ?>
								</div>
								
								<a href="<?php echo $aboutme_section['cta_link']; ?>" class="btn btn-success btn-outlined"><span><?php echo $aboutme_section['cta_text']; ?></span> <i class="<?php echo $aboutme_section['cta_icon']; ?>"></i></a>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="footer">
								<img src="<?php echo $aboutme_section['image']; ?>">
							</div>
						</div>
					</div>
					
					
				</div>
			</section>
			<!-- END: .about-me -->

			<?php 
				$portfolio_section = $customizer->getThemeOptions('portfolio_section');
			?>
			<!-- .portfolio -->
			<section class="row portfolio">
				<div class="col-md-12 col-lg-8 col-lg-offset-2">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-sm-push-6">
							<div class="head">
								<h1 class="title"><?php echo $portfolio_section['title']; ?><!-- Get a Dog Up Ya' --></h1>
							</div>
							<div class="body">
								<div class="content">
									<?php echo $portfolio_section['body']; ?>
								</div>
			
								<a href="<?php echo $portfolio_section['cta_link']; ?>" class="btn btn-danger btn-outlined"><span><?php echo $portfolio_section['cta_text']; ?></span> <i class="<?php echo $portfolio_section['cta_icon']; ?>"></i></a>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-sm-pull-6">
							<div class="footer">
								<img src="<?php echo $portfolio_section['image']; ?>">
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- END: .portfolio -->

			<?php 
				$donate_section = $customizer->getThemeOptions('donate_section');
			?>
			<!-- .donate -->
			<section class="row donate">
				<div class="col-md-12 col-lg-8 col-lg-offset-2">
					<div class="head">
						<h1 class="title"><?php echo $donate_section['title']; ?></h1>
					</div>
					<div class="body">
						<a href="<?php echo $donate_section['cta_link']; ?>" class="btn btn-default btn-outlined"><span><?php echo $donate_section['cta_text']; ?></span><i id="icon" class="<?php echo $donate_section['cta_icon']; ?>"></i></strong></a>
					</div>
				</div>
			</section>
			<!-- END: .donate -->
			
			<?php 
				$contact_section = $customizer->getThemeOptions('contact_section');
			?>
			<!-- .contact -->
			<section class="row contact">
				<div class="body">
					<div class="social-media">
						<a id="link1" href="<?php echo $contact_section['link1']; ?>" class="socmed">
							<i class="<?php echo $contact_section['icon1']; ?>"></i>
						</a>
						<a id="link2" href="<?php echo $contact_section['link2']; ?>" class="socmed">
							<i class="<?php echo $contact_section['icon2']; ?>"></i>
						</a>
						<a id="link3" href="<?php echo $contact_section['link3']; ?>" class="socmed">
							<i class="<?php echo $contact_section['icon3']; ?>"></i>
						</a>
						<a id="link4" href="<?php echo $contact_section['link4']; ?>" class="socmed">
							<i class="<?php echo $contact_section['icon4']; ?>"></i>
						</a>
					</div>
					<?php dynamic_sidebar('perth-homepage-widget') ?>
				</div>
			</section>
			<!-- END: .contact -->
		</main>
		<!-- END Home Main -->

		<?php endwhile; ?>
	<?php endif; ?>

	<?php 
		$footer = $customizer->getThemeOptions('footer_section');
	?>
	<!-- Home Footer -->
	<footer class="container-fluid">
		<div class="content">
			<?php echo $footer['content']; ?>
		</div>
		<div id="scroll_top" class="scroll-top">
			<i class="fas fa-chevron-up"></i>
		</div>
	</footer>
	<!-- END Home Footer -->

	<?php wp_footer(); ?>
</body>
</html>