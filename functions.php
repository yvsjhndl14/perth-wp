<?php 
/**
	@package perth-project-theme
	functions.php
	===================================
	INCLUDE FILES
	===================================
*/

// Base
require_once get_template_directory() . '/inc/class.perth_theme.php';

require_once get_template_directory() . '/inc/class.perth_theme_init.php';
require_once get_template_directory() . '/inc/class.perth_theme_support.php';
require_once get_template_directory() . '/inc/class.perth_shortcodes.php';
require_once get_template_directory() . '/inc/walkers/primary_nav.php';

// Widgets
require_once get_template_directory() . '/inc/widgets/newsletter.php';
require_once get_template_directory() . '/inc/class.perth_widget_loader.php';
require_once get_template_directory() . '/inc/class.perth_custom_functions.php';

// Customizer
require_once get_template_directory() . '/inc/controls/icon-selector-custom-control.php';
require_once get_template_directory() . '/inc/controls/text-editor-custom-control.php';
require_once get_template_directory() . '/inc/class.perth_customizer.php';

?>